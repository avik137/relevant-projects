//Avinash Kumar
//Security in Computing CIS 3360
//Hill cipher program

//Libraries
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

int main(int argc, char** argv){
    //Declared Variables
    char place;
    int block[9];
    int i,j,count,key_length,x,y,k;
    int key[9][9];
    int flag;
    //Declared input files and arrays
    FILE *ifs, *ifp;
    char* Key = argv[1];
    char* Plaintext = argv[2];
    //Used this test the file in C
    //char* Key = "key.txt";
    //char* Plaintext = "Plaintext.txt";
    ifs = fopen(Key, "r");
    ifp = fopen(Plaintext, "r");
    char at[10000];
    char bt[10000];
    int  ct[10000];
    i = 0;

    //Initalize the key text integer for the array
    for(i = 0; i < 10000; i++)
            ct[i] = 0;

    //Scans in the keys length from the text file
    fscanf(ifs,"%d",&key_length);
    printf("%d\n",key_length);

    //This loop scans in key suing the key length scanned in previously
    for(i = 0; i < key_length; i++){
        for(j = 0; j < key_length; j++){
            fscanf(ifs,"%d",&key[i][j]);
            printf("%d ",key[i][j]);
      }
        printf("\n");
    }
    printf("\n");

    //Resets i and j counters to use in the other loop
    i = 0;
    j = 0;

    //Loop to store the only the plain text
    while(!feof(ifp)){
        flag = 0;
        fscanf(ifp, "%c", &place);
        printf("%c",place);

        //store only lowercase letters
        if(place >= 97 && place <= 122){
            at[i] = place;
            flag = 1;
        }
        //Change upper case characters to lowercase
        else if(place >= 65 && place <= 90){
            at[i] = (place + 32);
            flag = 1;
        }
        //If character was stored correctly then go to next spot
        if(flag == 1)
            i++;
        }

    //Check if it needs padding when doing matrix multiplication
    count = i;
    int left = count % key_length;
    if(left != 0)
        //Pad with X's
        for(i = 0; i < key_length - left; i++){
            at[count] = 'x';
            count++;
        }

    //Encrypt the plain text with the key to get the cipher text
    for (i = 0; i < strlen(at); i += key_length) {
        for (j = 0; j < key_length; j++) {
            for (k = 0; k < key_length; k++) {
                ct[i + j] += (key[j][k] * (int)(at[i + k] - 97));
            }
            bt[i + j] = (char) ((ct[i+j] % 26) + 97);
        }
    }
    printf("\n\n");

    //Print outs the cipher text
    for(i = 0; i < strlen(bt);){
        for(j = 0; j < 80; j++){
            if(i < strlen(bt))
                printf("%c",bt[i++]);
        }
    }
     printf("\n");
}
