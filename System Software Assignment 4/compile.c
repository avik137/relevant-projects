/*
Group: Four People
Members:
        Daniel Belalcazar
        Avi Kumar
        Sarvesh Khemlani
        Mark Behler
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define MAX_SYMBOL_TABLE_SIZE 100
#define MAX_STACK_HEIGHT 2000


typedef enum
{
    LIT = 1, OPR, LOD, STO, CAL, INC, JMP, JPC, SIO1, SIO2, SIO3
}ISA;


//struct for the registers
typedef struct intruction{
int op;
int  l;
int  m;
}instruction;

instruction pmCode[MAX_STACK_HEIGHT];//stores emitted instructions


typedef struct symbol {
 int kind; // const = 1, var = 2, proc = 3
 char name[12]; // name up to 11 chars
 int val; // number (ASCII value)
 int level; // L level
 int addr; // M address
} symbol;

symbol symbol_table[MAX_SYMBOL_TABLE_SIZE];

//list of token types
typedef enum
{
    nulsym = 1, identsym, numbersym, plussym, minussym,
    multsym, slashsym, oddsym, eqsym, neqsym, lessym, leqsym,
    gtrsym, geqsym, lparentsym, rparentsym, commasym, semicolonsym,
    periodsym, becomessym, beginsym, endsym, ifsym, thensym,
    whilesym, dosym, callsym, constsym, varsym, procsym, writesym,
    readsym, elsesym
} tokenType;

//list of all the words from the input
typedef struct node{
    char* word;
    tokenType token;
    struct node* next;
    struct node* prev;
}node;


typedef struct token{
    tokenType type;
    char tokenName[12];
}token;


//function prototypes
void putSymbol(int kind, char name[12], int num, int level, int modifier,FILE* outputFile);
void statement(token tokenList[],FILE* outputFile);
void factor(token tokenList[],FILE* outputFile);
void block(token tokenList[],FILE* outputFile);
void term(token tokenList[],FILE* outputFile);
void condition(token tokenList[],FILE* outputFile);
void program(token tokenList[],FILE* outputFile);
void lexer(node* head,FILE* outputFile);
void insertNode(node** head, node** tail,FILE* outputFile);
void parseError(int e,FILE* outputFile);
void printTokenList(token tokenList[]);
node* startsWithLetter(FILE* input, char firstLetter, node** tail,FILE* outputFile);
node* startsWithDigit(FILE* input, char firstNumber, node** tail,FILE* outputFile);
node* startsWithSymbol(FILE* input, char firstSymbol, node** tail,FILE* outputFile);
symbol* findSymbol(char currentWord[12],FILE* outputFile);
int checkType(char word[12],FILE* outputFile);

int l;
int curToken;
int instructionCount;
int symCount;
int jTmp;
int increment;
int currentLocation;
int numTokens;

int main(int argc,char* argv[]){

    //open file from arguments twice in order
    FILE* inputFile=fopen(argv[1],"r");
    FILE* test=fopen(argv[1],"r");
    FILE* outputFile= fopen(argv[2], "w");

    //intializes linked list
    node* tail=NULL;
    node* head=NULL;

    //creates first node
    insertNode(&head, &tail,outputFile);

    //takes in the first char
    char firstC;
    firstC=fgetc(inputFile);



    //scans everything from file and prints it out
   // printf("source code:\n-------\n");
   // char fileInput[5000];
   // while(fgets(fileInput,5000, test))
       // printf("%s",fileInput);
    //printf("\n\n\n");

    //based off of whether the first char is a letter, number, or symbol treats it differently
    //printf("source code without comments:\n-------\n");
    while(firstC!=EOF){
        //if letter adds entire word to a node then gets then next char
        if(isalpha(firstC)){
            tail=startsWithLetter(inputFile, firstC, &tail,outputFile);
            insertNode(&head,&tail,outputFile);
            firstC=fgetc(inputFile);
        }
        //if number adds entire word to a node then gets then next char
        else if(isdigit(firstC)){
            tail=startsWithDigit(inputFile, firstC, &tail,outputFile);
            insertNode(&head,&tail,outputFile);
            firstC=fgetc(inputFile);
        }
         //if symbol adds entire word to a node then gets then next char
        else if(ispunct(firstC)){
            tail=startsWithSymbol(inputFile, firstC, &tail,outputFile);
            insertNode(&head,&tail,outputFile);
            firstC=fgetc(inputFile);
        }
        else{
         //if whitespace, prints it out to keep everything in order
            //printf("%c",firstC);
            firstC=fgetc(inputFile);
        }
    }

    //printf("\n\n\n");
    //printf("flag:\n-------\n");


    //stores token values for recursive descent parser
    int tokenCount=0;
    //tokenType tokenTable[tokenCount];
   // char* tokenName[MAX_SYMBOL_TABLE_SIZE];

    token tokenList[MAX_SYMBOL_TABLE_SIZE];

    //Goes through list to get each word in each node and finds the corresponding number for it
    while(head->next!=NULL){
        //if comment ignore it
        if(strlen(head->word)!= 0){
            //if the word inside the node doesnt contain an error print it out and find the corresponding value
            if(strstr(head->word,"Lexical")==NULL){


                   // tokenName[tokenCount]=head->word;
                    //printf("%s\t\t",tokenName[tokenCount]);

                    strcpy(tokenList[tokenCount].tokenName,head->word);
                    //printf("%d\t%s\t",tokenCount,tokenList[tokenCount].tokenName);

                    //printf("%s\t\t",head->word);
                    lexer(head,outputFile);

                    tokenList[tokenCount].type=head->token;
                    //printf("\t%d",tokenList[tokenCount].type);

                    //tokenTable[tokenCount]=head->token;
                    //printf("\t\t%d\t\t%d",tokenTable[tokenCount],tokenCount);

                    //printf("\n");
                    tokenCount++;

            }
            //if there is an error. print out the error type and stop the program
            if(strstr(head->word,"Lexical")!=NULL){
                    printf("%s\t\t",head->word);
                    printf("\n");
                    exit(1);
                }
        }

        //continue to next word
        head=head->next;
    }

    //printf("\n\n");

    numTokens=tokenCount;
   // printTokenList(tokenList);

    program(tokenList, outputFile);

    int i=0;
    /*for (i=0; i<symCount; i++){
            printf("%s\t", symbol_table[i].name);
            printf("%d\t", symbol_table[i].level);
            printf("%d\t", symbol_table[i].val);
            printf("%d\n", symbol_table[i].addr);
    }

    i=0;
    */
    for(i=1; i<instructionCount; i++)
        fprintf(outputFile,"%d %d %d\n",pmCode[i].op,  pmCode[i].l, pmCode[i].m);

    printf("No errors, program is syntactically correct\n");
    //close file
    fclose(outputFile);
    fclose(inputFile);
    fclose(test);
    return 0;
}







void printTokenList(token tokenList[]){

    printf("\nTokens inside token list:\n");
    int j=0;
    for(j=0;j<numTokens;j++){
        printf("%d\t%s\t",j, tokenList[j].tokenName);
        printf("%d\n",tokenList[j].type);
    }
    printf("Number of tokens: %d\n",numTokens);
    printf("\n");
}

void parseError(int e,FILE* outputFile){
    if (e == 1){
        printf("\nUsed '=' instead of ':='");
    }
    else if (e == 2){
        printf("\n'=' must be followed by a number");
    }
    else if (e == 3){
        printf("\nIdentifier must be followed by '=' if const or ':=' if variable");
    }
    else if (e == 4){
        printf("\nThe resreved words const, var, or procedure must be followed by identifier");
    }
    else if (e == 5){
        printf("\nSemicolon or comma expected");
    }
    else if (e == 6){
        printf("\nPeriod missing");
    }
    else if (e == 7){
        printf("\nUndeclared identifier");
    }
    else if (e == 8){
        printf("\nAssignment to constant or procedure not allowed");
    }
    else if (e == 9){
        printf("\nReserved word call must be followed by identifier");
    }
    else if (e == 10){
        printf("\nCannot call constant or variable");
    }
    else if (e == 11){
        printf("\nReserve word: Then expected");
    }
    else if (e == 12){
        printf("\nReserved word: do expected");
    }
    else if (e == 13){
        printf("\nRelational operator expected");
    }
    else if (e == 14){
        printf("\nExpression must not contain reserved word: procedure");
    }
    else if (e == 15){
        printf("\nRight parenthesis expected");
    }
    else if (e == 16){
        printf("\nNumber exceeds maximum limit");
    }
    else if (e == 17){
        printf("\nSemicolon between statements missing");
    }
    else if (e == 18){
        printf("\nMissing semicolon after statement in block");
    }
    else if (e == 19){
        printf("\nReserved word end expected");
    }
    else if (e == 20){
        printf("\nRead and Write must be followed by identifier");
    }
    else if (e == 22){
        printf("\nError in factor");
    }

    printf("\n");

    exit(1);
}

int checkType(char word[12],FILE* outputFile)//finds the type the symbol is
{
    int i;
    for (i=0; i<symCount; i++){
        if (strcmp(word, symbol_table[i].name) == 0){
            return symbol_table[i].kind;//return the type
        }
    }

    parseError(7,outputFile);
    return 0;
}

symbol* findSymbol(char currentWord[12],FILE* outputFile){
    //printf("\nName: %s\n", currentWord);
    int i;

    //this next loop just takes the next symbol sharing the same name

    for (i=0; i<symCount; i++){
        if (strcmp(currentWord, symbol_table[i].name) == 0)
        {
          /*  printf("\nInstruction: %d\t\t", instructionCount);
            printf("%s\t", symbol_table[i].name);
            printf("%d\t", symbol_table[i].level);
            printf("%d\t", symbol_table[i].val);
            printf("%d\n", symbol_table[i].addr);
*/
            return &symbol_table[i];
        }
    }
    parseError(7,outputFile);
    return 0;
}

void putSymbol(int kind, char name[12], int num, int level, int modifier,FILE* outputFile){
    symbol newSymbol;

    //printf("kind:%d \tname:%s \tnum:%d \tlvl:%d \tmVal:%d\n",kind, name, num, level, modifier);

    newSymbol.kind=kind;
    newSymbol.level=level;
    newSymbol.val=num;
    newSymbol.addr=modifier;
    strcpy(newSymbol.name,name);
    symbol_table[symCount]=newSymbol;
    symCount++;
    return;

}

void emit(int op, int level, int addr,FILE* outputFile){
    //printf("%d\t%d %d %d\n",instructionCount, op, level, addr);

    //set the values in the mcode array
    pmCode[instructionCount].op = op;
    pmCode[instructionCount].l = level;
    pmCode[instructionCount].m = addr;

    instructionCount++;
}


void program(token tokenList[],FILE* outputFile){
    curToken=0;
    symCount=0;
    instructionCount=1;
    l=-1;
    // printTokenList(tokenList);


    block(tokenList,outputFile);
   // printf("\nCurrent token value: %d\n",curToken);

    if(tokenList[curToken].type!=periodsym)
        parseError(6,outputFile);
    emit(SIO1, 0, 2,outputFile);

  //  printTokenList(tokenList);
}

void block(token tokenList[],FILE* outputFile){
    int numVars=0;
    int m=4;
    l++;
    int jumpAddress=instructionCount;
    emit(JMP,0,0,outputFile);


    if(tokenList[curToken].type==constsym){
        do{
            char constName[MAX_SYMBOL_TABLE_SIZE];
            strcpy(constName,tokenList[curToken+1].tokenName);

            curToken++;
            if(tokenList[curToken].type!=identsym)parseError(4,outputFile);
            curToken++;

            int constVal= atoi(tokenList[curToken+1].tokenName);
            putSymbol(1, constName , constVal, l, constVal,outputFile);

            if(tokenList[curToken].type!=eqsym)parseError(3,outputFile);
            curToken++;
            if(tokenList[curToken].type!=numbersym)parseError(2,outputFile);
            curToken++;
        }while(tokenList[curToken].type==commasym);

        if(tokenList[curToken].type!=semicolonsym)parseError(5,outputFile);
        curToken++;
    }

    if(tokenList[curToken].type==varsym){
        do{
            //if(findSymbol(tokenList[curToken].tokenName))printf("error!!");
            numVars++;
            m++;
            curToken++;

            if(tokenList[curToken].type!=identsym)parseError(4,outputFile);
            char varName[MAX_SYMBOL_TABLE_SIZE];
            strcpy(varName,tokenList[curToken].tokenName);
            putSymbol(2, varName, 0, l, 3+numVars,outputFile);
            curToken++;
        }while(tokenList[curToken].type==commasym);

        if(tokenList[curToken].type!=semicolonsym) parseError(5,outputFile);
        curToken++;
    }

    while(tokenList[curToken].type==procsym){
        curToken++;

        char procName[MAX_SYMBOL_TABLE_SIZE];
        strcpy(procName,tokenList[curToken].tokenName);
        putSymbol(3,tokenList[curToken].tokenName, 0, l, instructionCount,outputFile);

        if(tokenList[curToken].type!=identsym)
            parseError(4,outputFile);
        curToken++;
        if(tokenList[curToken].type!=semicolonsym)
            parseError(5,outputFile);
        curToken++;
        block(tokenList,outputFile);
        if(tokenList[curToken].type!=semicolonsym)
            parseError(18,outputFile);
        curToken++;
    }

    pmCode[jumpAddress].m=instructionCount-1;
    emit(INC, 0, numVars+4,outputFile);
    statement(tokenList,outputFile);

    if(l>0)
        emit(OPR, 0, 0,outputFile);
    l--;
}


void expression(token tokenList[],FILE* outputFile){

    if(tokenList[curToken].type==plussym
       || tokenList[curToken].type==minussym)
    {
        curToken++;
        term(tokenList,outputFile);
        emit(OPR,0,1,outputFile);
    }
    else
        term(tokenList,outputFile);

    while(tokenList[curToken].type==plussym
          ||tokenList[curToken].type==minussym)
    {
        int mTemp;
        if(tokenList[curToken].type==plussym)
            mTemp=2;
        else
            mTemp=3;
        curToken++;
        term(tokenList,outputFile);
        emit(OPR,0,mTemp,outputFile);
    }
}


void statement(token* tokenList,FILE* outputFile){
    int temp1, temp2, jumpTemp;

    if(tokenList[curToken].type==identsym){
        char curWord[12];
        strcpy(curWord,tokenList[curToken].tokenName);
        symbol* curSymbol= findSymbol(curWord,outputFile);

        if (!curSymbol)
        {
            printf("tried to print null pointer2");
            exit(1);
        }
        //putSymbol(2, curWord, 0, 0, 3+numVars);
        curToken++;
        if(tokenList[curToken].type!=becomessym){
            if(tokenList[curToken].type==eqsym)
                parseError(1,outputFile);
            else{
                parseError(3,outputFile);
            }
        }
        curToken++;
        expression(tokenList,outputFile);
        emit(STO,l-curSymbol->level,curSymbol->addr,outputFile);
    }

    else if(tokenList[curToken].type==callsym){
        curToken++;

        char curWord[12];
        strcpy(curWord,tokenList[curToken].tokenName);
        symbol* curSymbol= findSymbol(curWord,outputFile);

        if (!curSymbol)
        {
            printf("tried to print null pointer3");
            exit(1);
        }
        if(tokenList[curToken].type!=identsym)
            parseError(9,outputFile);

        emit(CAL,l-curSymbol->level,curSymbol->addr-1,outputFile);
        curToken++;
    }

    else if(tokenList[curToken].type==beginsym){
        curToken++;

        statement(tokenList,outputFile);
        while(tokenList[curToken].type==semicolonsym){
            curToken++;
               //     printf("current index:%d\tToken Name:%s\tToken Value:%d\n", curToken, tokenList[curToken].tokenName,tokenList[curToken].type);

            statement(tokenList,outputFile);
        }
        if(tokenList[curToken].type!=endsym)
             parseError(19,outputFile);

        curToken++;
    }

    else if(tokenList[curToken].type==ifsym){

        curToken++;
       // int mTemp=curToken;
        condition(tokenList,outputFile);
       //printf("current index:%d\tToken Name:%s\tToken Value:%d\n", curToken, tokenList[curToken].tokenName,tokenList[curToken].type);

        if(tokenList[curToken].type!=thensym)
            parseError(11,outputFile);

        curToken++;

        jumpTemp=instructionCount;
        emit(JPC,0,0,outputFile);
        statement(tokenList,outputFile);

        pmCode[jumpTemp].m=instructionCount-1;

        if(tokenList[curToken].type==elsesym){
            curToken++;
            temp2=instructionCount;


            emit(JMP,0,tokenList[curToken-1].type,outputFile);
            pmCode[jumpTemp].m=instructionCount-1;

            statement(tokenList,outputFile);
            pmCode[temp2].m=instructionCount-1;
        }
        else
            pmCode[jumpTemp].m=instructionCount-1;

    }
    else if(tokenList[curToken].type==whilesym){

        temp1=instructionCount;
        curToken++;
        condition(tokenList,outputFile);
        temp2=instructionCount;


        emit(JPC,0,temp2+9,outputFile);

        if(tokenList[curToken].type!=dosym)
            parseError(12,outputFile);
        else
            curToken++;

        statement(tokenList,outputFile);

        emit(JMP,0,temp1-1,outputFile);

        pmCode[temp2].m=instructionCount-1;
    }
    else if(tokenList[curToken].type==readsym)
    {
        curToken++;

        if(tokenList[curToken].type!=identsym)parseError(20,outputFile);
        char curWord[12];
        strcpy(curWord,tokenList[curToken].tokenName);
        symbol* curSymbol= findSymbol(curWord,outputFile);

         if (!curSymbol)
        {
            printf("you still have this variable undeclared");
            exit(1);
        }

        emit(SIO1,0,1,outputFile);
        emit(STO,l-curSymbol->level,curSymbol->addr,outputFile);
        curToken++;
    }
    else if(tokenList[curToken].type==writesym)
    {
        curToken++;
        if(tokenList[curToken].type!=identsym)
            parseError(20,outputFile);

        char curWord[12];
        strcpy(curWord,tokenList[curToken].tokenName);
        symbol* curSymbol= findSymbol(curWord,outputFile);

        if (!curSymbol)
        {
            printf("declare variable");
            exit(1);
        }
        if(curSymbol->kind==2)
            emit(LOD,l-curSymbol->level,curSymbol->addr,outputFile);
        else if(curSymbol->kind==1)
            emit(LIT,0,curSymbol->val,outputFile);
        emit(SIO1, 0, 0,outputFile);
        curToken++;
    }
}

void condition(token tokenList[],FILE* outputFile){

    if(tokenList[curToken].type==oddsym){
        curToken++;
        expression(tokenList,outputFile);
        emit(OPR,0,6,outputFile);
    }
    else{

        expression(tokenList,outputFile);

        if(tokenList[curToken].type != lessym
            && tokenList[curToken].type != leqsym
            && tokenList[curToken].type != gtrsym
            && tokenList[curToken].type != geqsym
            && tokenList[curToken].type != eqsym
            && tokenList[curToken].type != neqsym)parseError(13,outputFile);

        int mTemp=tokenList[curToken].type;
        curToken++;

        expression(tokenList,outputFile);

        emit(OPR,0,mTemp-1,outputFile);
    }
}


void term(token tokenList[],FILE* outputFile){

    factor(tokenList,outputFile);
    while(tokenList[curToken].type==multsym||tokenList[curToken].type==slashsym){
        int mTemp=tokenList[curToken].type;
        curToken++;
        factor(tokenList,outputFile);
        emit(OPR,0,mTemp-2,outputFile);
    }
}

void factor(token tokenList[],FILE* outputFile){

    if(tokenList[curToken].type==identsym){
        char curWord[12];
        strcpy(curWord,tokenList[curToken].tokenName);
        symbol* curSymbol= findSymbol(curWord,outputFile);
        if (!curSymbol)
        {
            printf("tried to print null pointer6");
            exit(1);
        }
        if(curSymbol->kind==2)
            emit(LOD,l-curSymbol->level,curSymbol->addr,outputFile);
        else if(curSymbol->kind==1)
            emit(LIT,0,curSymbol->val,outputFile);
        curToken++;

    }
    else if(tokenList[curToken].type==numbersym){
        int num= atoi(tokenList[curToken].tokenName);
        emit(LIT,0,num,outputFile);
        curToken++;
    }
    else if(tokenList[curToken].type==lparentsym){
        curToken++;
        expression(tokenList,outputFile);
        if(tokenList[curToken].type!=rparentsym)parseError(15,outputFile);
        curToken++;
    }
    else
        parseError(22,outputFile);
}


//based on the input prints out the corresponding value and stores value
void lexer(node* head,FILE* outputFile){
    if(isalpha(head->word[0])){
        if(strcmp(head->word,"odd")==0){
               // printf("8");
                head->token=oddsym;
        }
        else if(strcmp(head->word,"begin")==0){
                //printf("21");
                head->token=beginsym;
        }
        else if(strcmp(head->word,"end")==0){
                //printf("22");
                head->token=endsym;
        }
        else if(strcmp(head->word,"if")==0){
        //        printf("23");
                head->token=ifsym;
        }
        else if(strcmp(head->word,"then")==0){
          //      printf("24");
                head->token=thensym;
        }
        else if(strcmp(head->word,"while")==0){
            //    printf("25");
                head->token=whilesym;
        }
        else if(strcmp(head->word,"do")==0){
              //  printf("26");
                head->token=dosym;
        }
        else if(strcmp(head->word,"call")==0){
                //printf("27");
                head->token=callsym;
        }
        else if(strcmp(head->word,"const")==0){
                //printf("28");
                head->token=constsym;
        }
        else if(strcmp(head->word,"var")==0){
                //printf("29");
                head->token=varsym;
        }
        else if(strcmp(head->word,"procedure")==0){
                //printf("30");
                head->token=procsym;
        }
        else if(strcmp(head->word,"write")==0){
                //printf("31");
                head->token=writesym;
        }
        else if(strcmp(head->word,"read")==0){
                //printf("32");
                head->token=readsym;
        }
        else if(strcmp(head->word,"else")==0){
                //printf("33");
                head->token=elsesym;
        }
        else{
            //printf("2");
            head->token=identsym;
        }
    }
    else if(isdigit(head->word[0])){
        //printf("3");
        head->token=numbersym;
    }
    else if(ispunct(head->word[0])){
        switch(head->word[0]){
            case '+':
          //      printf("4");
                head->token=plussym;
                break;
            case '-':
            //    printf("5");
                head->token=minussym;
                break;
            case '*':
              //  printf("6");
                head->token=multsym;
                break;
            case '/':
               // printf("7");
                head->token=slashsym;
                break;
            case '=':
                //printf("9");
                head->token=eqsym;
                break;


            case '<':
                if(strlen(head->word)!=1){
                    if(strcmp(head->word,"<>")==0){
                  //      printf("10");
                        head->token=neqsym;
                    }
                    else if(strcmp(head->word,"<=")==0){
                    //    printf("12");
                        head->token=leqsym;
                    }
                }
                else{
                    //printf("11");
                    head->token=lessym;
                }
                break;

            case '>':
                if(strlen(head->word)!=1){
                        if(strcmp(head->word,">=")==0){
                      //      printf("14");
                            head->token=geqsym;
                        }
                }
                else{
                        //printf("13");
                        head->token=gtrsym;
                }
                break;

            case '(':
                //printf("15");
                head->token=lparentsym;
                break;
            case ')':
                //printf("16");
                head->token=rparentsym;
                break;
            case ',':
                //printf("17");
                head->token=commasym;
                break;
            case ';':
                //printf("18");
                head->token=semicolonsym;
                break;
            case '.':
                //printf("19");
                head->token=periodsym;
                break;
            case ':':
                //printf("20");
                head->token=becomessym;
                break;
            default:
                printf("Error: Invlaid Token");
                break;
        }
    }
}


//if letter, makes char array
node* startsWithLetter(FILE* input, char firstLetter, node** tail,FILE* outputFile){
    int nextLetter,len=11;
    char* word=calloc(len+1,sizeof(char));
    int i=1;

    word[0]=firstLetter;
    nextLetter=fgetc(input);

    //while next char is letter or number keeps scanning to make char array
    while(isalpha(nextLetter)||isdigit(nextLetter)){
        if(i>=len){
            len*=2;
            word=realloc(word,len+1);
        }

        word[i]=nextLetter;
        i++;
        nextLetter=fgetc(input);
    }

    if (nextLetter != EOF )
        fseek(input, -1, SEEK_CUR);

    //used to print input without comments
    //printf("%s",word);

    //checks if string is too long to be identifier
    if(strlen(word)<13){
        (*tail)->word=word;
    }
     else{
        (*tail)->word="(Lexical Error: Invalid Identifier)";
    }
    return *tail;

}

//if number, makes char array
node* startsWithDigit(FILE* input, char firstNumber, node** tail,FILE* outputFile){
    int nextNumber;
    int len=5;
    char* intArray=calloc(len+1,sizeof(int));
    int i=1;

    intArray[0]=firstNumber;
    nextNumber=fgetc(input);

    //while the next input is an int or char keep adding to the string
    while(isdigit(nextNumber)||isalpha(nextNumber)){
         if(i>=len){
            len*=2;
            intArray=realloc(intArray,len+1);
        }

        intArray[i]=nextNumber;
        i++;
        nextNumber=fgetc(input);
    }

    //Goes back a character in file so the non numbers can be read in
    if (nextNumber != EOF )
        fseek(input, -1, SEEK_CUR);

    //checks what was just scanned in. if the char array starts w number but then scans in letters returns error
    //printf("%s",intArray);
    i=0;
    while(i<strlen(intArray)){
        if(isalpha(intArray[i])){
               (*tail)->word="(Lexical Error: Identifier doesnt start with a letter)";
                return *tail;
        }
        i++;
    }

    int check;
    sscanf(intArray, "%d", &check);

    //checks if the number exceeds max number permitted
    if(check<65535){
        (*tail)->word=intArray;
    }
    else{
        (*tail)->word="(Lexical Error: Number is greater than 65535)";
    }

    return *tail;
}

//if symbol, makes char array
node* startsWithSymbol(FILE* input, char firstSymbol, node** tail,FILE* outputFile){
    int len=3;

    char* symbols=calloc(len+1,sizeof(char));
    char nextSymbol;
    symbols[0]=firstSymbol;

    //if comment ignore it by setting flag
    switch(firstSymbol){
        case '/' :
            nextSymbol=fgetc(input);
            if(nextSymbol=='*'){
                 do{
                    while (nextSymbol != '*' ){
                            printf(" ");
                            nextSymbol = fgetc(input);
                    }
                    printf(" ");
                    nextSymbol = fgetc(input);
                    }while(nextSymbol != '/' );

                (*tail)->word="\0";
                return *tail;
            }

            else{
                if ( nextSymbol != EOF )
                    fseek(input, -1, SEEK_CUR);
            }
            break;


        //checks for < <> <=
        case '<' :
            nextSymbol=fgetc(input);
            if(nextSymbol=='='||nextSymbol=='>'){
                symbols[1]=nextSymbol;
            }
            else{
                  if ( nextSymbol != EOF )
                    fseek(input, -1, SEEK_CUR);
            }
            break;

        case '>' :

            //checks for >=
            nextSymbol=fgetc(input);
            if(nextSymbol=='='){
                symbols[1]=nextSymbol;
            }
            else{
            //checks for >
                if ( nextSymbol != EOF )
                    fseek(input, -1, SEEK_CUR);
            }
            break;

        //checks for :=
        case ':' :
           nextSymbol=getc(input);
            if(nextSymbol=='='){
                symbols[1]=nextSymbol;
            }
           else{
                symbols[1]=nextSymbol;
               // printf("%s",symbols);
                (*tail)->word="\0";
                return *tail;
            }
             break;
    //checks for rest
        case '+' :
        case '-' :
        case '*' :
        case '(' :
        case ')' :
        case '=' :
        case ',' :
        case '.' :
        case ';' :
            break;
        default:
            //if not desired symbol return error message
            (*tail)->word="(Lexical Error: Invalid Token)";
            printf("%s",symbols);
            return *tail;
    }

    //if crazy number of symbols return error
    if(strlen(symbols)<3){
            (*tail)->word=symbols;
           // printf("%s",(*tail)->word);

    }
   else{
        (*tail)->word="(Lexical Error: Invalid Token)";
        //printf("(Lexical Error: Invalid Token)");
    }
    return *tail;
}



//used to make linked list
void insertNode(node** head, node** tail,FILE* outputFile){
    if(*head==NULL){
        *head=(node*)malloc(sizeof(node));
        (*head)->next=NULL;
        (*head)->prev=NULL;
        *tail=*head;
    }
    else{

        (*tail)->next=(node*)malloc(sizeof(node));
        (*tail)->prev=*tail;
        *tail=(*tail)->next;
        (*tail)->next=NULL;
    }
}


