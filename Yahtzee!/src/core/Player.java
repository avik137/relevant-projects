//Avinash Kumar
package core;

//THis is for players of the yahtzee game
public class Player {
    
    //Constants
    private String name;
    private ScoreCard score;
    private Roll roll;
    
    public Player()
    {
        roll = new Roll();
    }
    
    public String getname()
    {
        return name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public ScoreCard getScoreCard()
    {
        return score;
    }
    
    public void setScoreCard(ScoreCard score)
    {
        this.score = score;
    }
    
    public Roll getRoll()
    {
        return roll;
    }
    
    public void setRoll(Roll roll)
    {
        this.roll = roll;
    }
    
    public void rollDice()
    {
        roll.rollDice();
    }
    
}
