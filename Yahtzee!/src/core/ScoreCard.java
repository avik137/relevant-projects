// Avinash Kumar
package core;

// This is the part where we record the scores of the players
public class ScoreCard {
    LowerSection lower = new LowerSection();
    UpperSection upper = new UpperSection();
    int grandTotal;
    
 public LowerSection getlowerSection(){
        return this.lower;
    }
     public void setlowerSection(LowerSection l){
        this.lower = l;
    }
    
 public UpperSection getUpperSection(){
        return this.upper;
    }  
    public void setUpperSection(UpperSection u){
        this.upper = u;
    }
     public int getgrandTotal(){
        return this.grandTotal;
    }
    public void setgrandTotal(int x){
        this.grandTotal = x;
    }    
}
