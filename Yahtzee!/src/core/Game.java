//Avinash Kumar
package core;

import java.util.*;

public class Game {
    
    //Constants
    private int gameTurn;
    private ArrayList<Player> players = new ArrayList<Player>();
    private Scanner input = new Scanner(System.in);
    
    public Game()
    {
        //Output
        System.out.print("How many players for this game? ");
        //Constant
        int numplayers = input.nextInt();
        //Loop For how many players there are 
        for(int i = 0; i < numplayers; i++)
        {
            newPlayer();
        }    
        System.out.println();
    }
       
    public int getGameTurn() {
        return gameTurn;
    }

    public void setGameTurn(int gameTurn) {
        this.gameTurn = gameTurn;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public Scanner getInput() {
        return input;
    }

    public void setInput(Scanner input) {
        this.input = input;
    }
       
    public void newPlayer()
    {
        //Where we enter teh different players name
        System.out.print("Enter player's name: ");
        players.add(new Player());
        players.get(players.size()-1).setName(input.next());
    }
    
    public void displayPlayers()
    {
        //Displaying the different players the user inputted
        System.out.println("Players for this game are:");
        //Loop to print the names
        for(Player player : players)
            System.out.println(player.getname());
            System.out.println();
    }
    
    public void playGame()
    {
        //Loop to print the dice rolls of each player
        for(Player player : players)
        {
            System.out.println("Player " + player.getname() + " rolled:");
            player.rollDice();
        }
    }
    
    
}
