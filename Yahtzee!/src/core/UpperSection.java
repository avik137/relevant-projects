// Avinash Kumar
package core;

//This the part where the scores depend on the number the plaer rolled
public class UpperSection {
    
    private int aces;

    public int getAces() {
        return aces;
    }
    public void setAces(int aces) {
        this.aces = aces;
    }
    public int getTwos() {
        return twos;
    }
    public void setTwos(int twos) {
        this.twos = twos;
    }
    public int getThrees() {
        return threes;
    }
    public void setThrees(int threes) {
        this.threes = threes;
    }
    public int getFours() {
        return fours;
    }
    public void setFours(int fours) {
        this.fours = fours;
    }
    public int getFives() {
        return fives;
    }
    public void setFives(int fives) {
        this.fives = fives;
    }
    public int getSixes() {
        return sixes;
    }
    public void setSixes(int sixes) {
        this.sixes = sixes;
    }
    public int getTotalScore() {
        return totalScore;
    }
    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }
    public int getBonus() {
        return bonus;
    }
    public void setBonus(int bonus) {
        this.bonus = bonus;
    }
    //Constants
    private int twos;
    private int threes;
    private int fours;
    private int fives;
    private int sixes;
    private int totalScore;
    private int bonus;
}
