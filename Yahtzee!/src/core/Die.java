//Avinash Kumar
package core;

//Randommizer 
import java.util.Random;

public class Die {
    
    //Constant
    int faceValue;
    
    // Makes the die so that we can run it in a loop for the game
    public void rollDie(){
        Random random = new Random();
        faceValue = (1 + random.nextInt(6));   
    }
    
    //Getter
    public String getfaceValue(){
        return Integer.toString(this.faceValue);
    }
    
    //Setter
    public void setfaceValue(int value){
        this.faceValue = value;
    }
    
    //String
    public String toString(){
        return String.valueOf(this.faceValue);
    }
}