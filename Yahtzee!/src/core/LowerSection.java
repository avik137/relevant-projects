//Avinash Kumar
package core;

// The lower selection is the part of the scoreboard where 
// all the special type of scores going that arent just single numbers.
public class LowerSection {
    
    private int threeKind;
    private int fourKind;

    public int getThreeKind() {
        return threeKind;
    }

    public void setThreeKind(int threeKind) {
        this.threeKind = threeKind;
    }

    public int getFourKind() {
        return fourKind;
    }

    public void setFourKind(int fourKind) {
        this.fourKind = fourKind;
    }

    public int getFullHouse() {
        return fullHouse;
    }

    public void setFullHouse(int fullHouse) {
        this.fullHouse = fullHouse;
    }

    public int getSmStraight() {
        return smStraight;
    }

    public void setSmStraight(int smStraight) {
        this.smStraight = smStraight;
    }

    public int getLgStraight() {
        return lgStraight;
    }

    public void setLgStraight(int lgStraight) {
        this.lgStraight = lgStraight;
    }

    public int getYahtzee() {
        return yahtzee;
    }

    public void setYahtzee(int yahtzee) {
        this.yahtzee = yahtzee;
    }

    public int getChance() {
        return chance;
    }

    public void setChance(int chance) {
        this.chance = chance;
    }

    public int getYahtzeeBonus() {
        return yahtzeeBonus;
    }

    public void setYahtzeeBonus(int yahtzeeBonus) {
        this.yahtzeeBonus = yahtzeeBonus;
    }

    public int getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(int totalScore) {
        this.totalScore = totalScore;
    }
    //Constantss
    private int fullHouse;
    private int smStraight;
    private int lgStraight;
    private int yahtzee;
    private int chance;
    private int yahtzeeBonus;
    private int totalScore;
    
     

}
