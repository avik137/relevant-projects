// Avinash Kumar
package core;

import java.util.*;

//This is to roll the die we made before
public class Roll {

    //Constant of the array to put the die results in
    private ArrayList<Die> dice = new ArrayList<Die>();

    public ArrayList<Die> getDice() {
        return dice;
    }

    public void setDice(ArrayList<Die> dice) {
        this.dice = dice;
    }
    
    public Roll()
    {
        initializeDice();
    }
    
    public void initializeDice()
    {
        //This loop is to add the dies for the dice rolls
        for(int i = 0; i < 5; i++)
            dice.add(new Die());    
    }
    
    public void rollDice()
    {   
        //This loop rolls the die 5 times and saves the data
        for (int i = 0; i < dice.size(); i++) 
        {
            dice.get(i).rollDie();
            System.out.println("Die " + (i+1) + " rolled value of " + dice.get(i).toString());
	}
        System.out.println();
    }
}
