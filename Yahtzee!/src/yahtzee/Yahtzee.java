//Avinash Kumar
//Teacher - Karin Whiting
//COP 3330 -15
package yahtzee;

import core.*;
import userInterface.YahtzeeUi;

//Puts all the parts together or known as the main program
public class Yahtzee {


    public static void main(String[] args) {

        YahtzeeUi yahtzee = new YahtzeeUi();
        yahtzee.initComponents();
        
        Game game = new Game();
        game.displayPlayers();
        game.playGame();
    }
    
}
