//Avinash Kumar
package userInterface;

//Imported classes
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

public class PlayerUi extends JPanel {
    
    JLabel playerName;
    JLabel playerScore;
    GridLayout grid;
    
    public PlayerUi()
    {
        initComponents();
    }
    
    public void initComponents()
    {
        //set layout to grid
       grid = new GridLayout(1,1);
       setLayout(grid);
       setBorder(new BevelBorder(BevelBorder.RAISED));
       
       //player ui labels
       playerName = new JLabel("Name ");
       add(playerName);
       playerScore = new JLabel("0");
       add(playerScore);
    }
             
}
