//Avinash Kumar
package userInterface;

//Imported LIbraries
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

public class GameUi extends JPanel{
    
    //Constants
    JLabel round;
    JLabel gameTurn;
    JLabel logo;
    FlowLayout flow;
    
    public GameUi() {
        initComponents();
    }
    
    public void initComponents() {
        //Flow is set up to set up the layout of the interface
        flow = new FlowLayout(FlowLayout.LEADING, 1, 100);
        setLayout(flow);
        setBorder(new BevelBorder(BevelBorder.RAISED));

        //Label
        logo = new JLabel("Yahtzee                     ");
        add(logo);
        
        //Records the rounds the user has playeed
        round = new JLabel("0");
        add(round);
        
        //Shows the number of turns left for that player in this round
        gameTurn = new JLabel("/13");
        add(gameTurn);
    }
}