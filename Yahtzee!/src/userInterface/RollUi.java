//Avinash Kumar
package userInterface;

//Imported LIbraries
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

//Sets up the Roll part of the interface for the dice and the roll button
public class RollUi extends JPanel{
    
    //Constants
    int currentDieSelected = 0;
    ArrayList<JButton> dice;
    JButton roll;
    BoxLayout box;
    
    public RollUi() {
        initComponents();
    }
    
    public void initComponents()
    {
        dice = new ArrayList<JButton>();
        box = new BoxLayout(this, BoxLayout.X_AXIS);
        setLayout(box);
        setBorder(new BevelBorder(BevelBorder.RAISED));
        
        //instantiate and add the five dice and the roll button 
        dice.add(new JButton("1"));
        add(dice.get(dice.size()-1));
        dice.get(dice.size()-1).addActionListener(new whichDie());
        
        dice.add(new JButton("2"));
        add(dice.get(dice.size()-1));
        dice.get(dice.size()-1).addActionListener(new whichDie());
        
        dice.add(new JButton("3"));
        add(dice.get(dice.size()-1));
        dice.get(dice.size()-1).addActionListener(new whichDie());
        
        dice.add(new JButton("4"));
        add(dice.get(dice.size()-1));
        dice.get(dice.size()-1).addActionListener(new whichDie());
        
        dice.add(new JButton("5"));
        add(dice.get(dice.size()-1));
        dice.get(dice.size()-1).addActionListener(new whichDie());
           
        roll = new JButton("Roll Dice");
        add(roll);
        roll.addActionListener(new whichDie());
           
    }   
    
    static class whichDie implements ActionListener{
        private int currentDieSelected;
        JFrame frame = new JFrame();
  
  // Function for the actions part so pressing the buttons performs specific actions
  public void actionPerformed (ActionEvent e){
      
      
    if(e.getActionCommand().equals("Roll Dice"))
     JOptionPane.showMessageDialog(frame, "Rolling the dice!");
    else if (e.getActionCommand().equals("1"))
      updateSelectedDie(currentDieSelected, 1);
        else if (e.getActionCommand().equals("2"))
      updateSelectedDie(currentDieSelected, 2);
        else if (e.getActionCommand().equals("3"))
      updateSelectedDie(currentDieSelected, 3);
        else if (e.getActionCommand().equals("4"))
      updateSelectedDie(currentDieSelected, 4);
        else if (e.getActionCommand().equals("5"))
      updateSelectedDie(currentDieSelected, 5);
        else if (e.getActionCommand().equals("6"))
      updateSelectedDie(currentDieSelected, 6);
    
  
   }
  
  public void updateSelectedDie(int current, int next){
      if(current == next){
          currentDieSelected = 0;
          JOptionPane.showMessageDialog(frame, "Die "+next+" is not selected");
      }
      else{
          currentDieSelected = next;
          JOptionPane.showMessageDialog(frame, "Selected die was "+next);
      }
  }
  
  }
}