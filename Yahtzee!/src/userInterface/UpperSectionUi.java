//Avinash Kumar
package userInterface;

//Imported LIbraries
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class UpperSectionUi extends JPanel{

    //Constants
    ArrayList<JButton> categories;
    int sections =0;
    ArrayList<JLabel> scores;
    JLabel total;
    JLabel bonus;
    JLabel totalScore;
    GridLayout grid;
    
    public UpperSectionUi()
    {
        initComponents();
    }
    
    public void initComponents()
    {
        categories = new ArrayList<JButton>();
        scores = new ArrayList<JLabel>();       
        grid = new GridLayout(9, 1);
        setLayout(grid);
        
        //instantiate and add the six categories of the upper section and their labels
        categories.add(new JButton());
        add(categories.get(categories.size()-1));
        
        categories.add(new JButton());
        add(categories.get(categories.size()-1));
        
        categories.add(new JButton());
        add(categories.get(categories.size()-1));
        categories.add(new JButton());
        add(categories.get(categories.size()-1));
        categories.add(new JButton());
        add(categories.get(categories.size()-1));
        categories.add(new JButton());
        add(categories.get(categories.size()-1));
        
        scores.add(new JLabel("Aces Count and add only Aces"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id = new Identify();
        id.setSection("Aces");
        categories.get(scores.size()-1).addActionListener(id);
        
        scores.add(new JLabel("Twos Count and add only Twos"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id1 = new Identify();
        id1.setSection("Twos");
        categories.get(scores.size()-1).addActionListener(id1);
        
        
        scores.add(new JLabel("Threes Count and add only Threes"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id2 = new Identify();
        id2.setSection("Threes");
        categories.get(scores.size()-1).addActionListener(id2);
        
        scores.add(new JLabel("Fours Count and add only Fours"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id3 = new Identify();
        id3.setSection("Fours");
        categories.get(scores.size()-1).addActionListener(id3);
        
        scores.add(new JLabel("Fives Count and add only Fives"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id4 = new Identify();
        id4.setSection("Fives");
        categories.get(scores.size()-1).addActionListener(id4);
        
        scores.add(new JLabel("Sixes Count and add only Sixes"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id5 = new Identify();
        id5.setSection("Sixes");
        categories.get(scores.size()-1).addActionListener(id5);
        
        //instantiate and add the score totals
        total = new JLabel("TOTAL of Upper Section -- 0");
        add(total);
        bonus = new JLabel("Bonus -- If total score is 63 or over");
        add(bonus);
        totalScore = new JLabel("TOTAL SCORE -- 0");
        add(totalScore);
        
    }
    
    //function for the action event 
    private class Identify implements ActionListener{
    
    private String string;

      
        public String getSection() {
            return string;
        }

        public void setSection(String string) {
            this.string = string;
        }
  
    //Function for the pop out part of the action
    public void actionPerformed (ActionEvent e){
      
      JFrame frame = new JFrame();
      JOptionPane.showMessageDialog(frame, "Selected category was "+getSection());
     
   }
  }  
              
}