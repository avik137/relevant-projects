//Avinash Kumar
package userInterface;

//Imported LIbraries
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class YahtzeeUi {
    //Constants 
    GameUi gameUi;
    PlayerUi playerUi;
    RollUi rollUi;
    ScoreCardUi scoreCardUi;
    JFrame frame;
    JMenuBar menuBar;
    JMenu game;
    JMenuItem exit;
    JMenuItem newGame;
    JPanel rightPanel;
    
    public YahtzeeUi()
    {
        initComponents();
    }
    
    public void initComponents()
    {
        gameUi = new GameUi();
        playerUi = new PlayerUi();
        rollUi = new RollUi();
        game = new JMenu("Yahtzee");
        exit = new JMenuItem("Exit");
        newGame = new JMenuItem("New Game");
        rightPanel = new JPanel();
        scoreCardUi = new ScoreCardUi();
        menuBar = new JMenuBar();

        //set the mnemonic key for menu items
        game.setMnemonic(KeyEvent.VK_Y);
        exit.setMnemonic(KeyEvent.VK_E);
        newGame.setMnemonic(KeyEvent.VK_N);
        menuBar.add(game);
        game.add(newGame);
        game.add(exit);
        newGame.addActionListener(new NewGame());
        exit.addActionListener(new Exit());
        
        
        //set frame layout and add a menu bar
        frame = new JFrame("Yahtzee");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new GridLayout(2, 3));
        frame.setJMenuBar(menuBar);
        
        rightPanel.setLayout(new GridLayout(3,1));
        rightPanel.add(gameUi);
        rightPanel.add(playerUi);
        rightPanel.add(rollUi);
     
        //adds the scorecards and right panel to frame
        frame.add(scoreCardUi.upperUi);
        frame.add(rightPanel);
        frame.add(scoreCardUi.lowerUi);
        
        frame.pack();
        frame.setVisible(true);
        
         
         
    }
    //function for the action event   
static class NewGame implements ActionListener{
  
  public void actionPerformed (ActionEvent e){
      
      JFrame frame = new JFrame();
      Object[] choices = {"Yes", "No"};
      
      JOptionPane.showOptionDialog(frame,"Are you sure you want to start a new game?", "Select an option",JOptionPane.YES_NO_CANCEL_OPTION ,JOptionPane.QUESTION_MESSAGE, null, choices, choices[0]);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

   }
  }  

static class Exit implements ActionListener{
  
  public void actionPerformed (ActionEvent e){
      
      System.exit(0);
   }
  }  
}
