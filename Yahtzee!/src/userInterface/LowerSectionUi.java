//Avinash Kumar
package userInterface;

//imported Libraries
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class LowerSectionUi extends JPanel {
    
    ArrayList<JButton> categories;
    ArrayList<JLabel> scores;
    JLabel totalLower;
    JLabel totalUpper;
    JLabel grandTotal;
    GridLayout grid;
    
    public LowerSectionUi()
    {
        initComponents();
    }
    
    public void initComponents()
    {
        grid = new GridLayout(11,1);
        setLayout(grid);
        
        categories = new ArrayList<JButton>();
        scores = new ArrayList<JLabel>();
        
        //adds eight different buttons
        for(int i=0; i<8; i++){
        categories.add(new JButton());
        add(categories.get(categories.size()-1));
        }
        
        //creates various buttons to add to the game and to the actionlisteners part of the UI
        scores.add(new JLabel("3 of a kind -- Add total of all die"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id = new Identify();
        id.setSection("3 of a kind");
        categories.get(scores.size()-1).addActionListener(id);
        
        scores.add(new JLabel("4 of a kind -- Add total of all dice"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id1 = new Identify();
        id1.setSection("4 of a kind");
        categories.get(scores.size()-1).addActionListener(id1);
        
        scores.add(new JLabel("Full house -- Score 25"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id2 = new Identify();
        id2.setSection("Full House");
        categories.get(scores.size()-1).addActionListener(id2);
        
        scores.add(new JLabel("Small straight -- Score 30"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id3 = new Identify();
        id3.setSection("Small straight");
        categories.get(scores.size()-1).addActionListener(id3);
        
        scores.add(new JLabel("Large straight -- Score 40"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id4 = new Identify();
        id4.setSection("Large straight");
        categories.get(scores.size()-1).addActionListener(id4);
        
        scores.add(new JLabel("YAHTZEE -- Score 50"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
        Identify id5 = new Identify();
        id5.setSection("YAHTZEE");
        categories.get(scores.size()-1).addActionListener(id5);
        
        scores.add(new JLabel("Chance -- Score total of all 5 dice"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
         Identify id6 = new Identify();
        id6.setSection("Chance");
        categories.get(scores.size()-1).addActionListener(id6);
        
        scores.add(new JLabel("YAHTZEE BONUS -- Score 100"));
        categories.get(scores.size()-1).add(scores.get(scores.size()-1));
           Identify id7 = new Identify();
        id7.setSection("YAHTZEE BONUS");
        categories.get(scores.size()-1).addActionListener(id7);
        
        totalLower = new JLabel("TOTAL of Upper Section -- 0");
        add(totalLower);
        
        totalUpper = new JLabel("TOTAL of Lower Section -- 0");
        add(totalUpper);
        
        grandTotal = new JLabel("GRAND TOTAL -- 0");
        add(grandTotal);
        
    }

// Action listener part to make the program actually function button
private class Identify implements ActionListener{
    
    private String string;

      
        public String getSection() {
            return string;
        }

        public void setSection(String string) {
            this.string = string;
        }
  
  public void actionPerformed (ActionEvent e){
      
      JFrame frame = new JFrame();
     JOptionPane.showMessageDialog(frame, "Selected category was "+getSection());
     
   }
  }  
        
}