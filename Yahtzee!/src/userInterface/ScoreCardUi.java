//Avinash Kumar
package userInterface;

//Imported LIbraries
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ScoreCardUi extends JPanel {
    
    //Constants
    JLabel grandTotal;
    LowerSectionUi lowerUi;
    UpperSectionUi upperUi;
    FlowLayout flow;
    
    public ScoreCardUi() {
        initComponents();
    }
    
    //Score card function
    public void initComponents() {
        grandTotal = new JLabel();
        lowerUi = new LowerSectionUi();
        upperUi = new UpperSectionUi();
        flow = new FlowLayout();
    }
}