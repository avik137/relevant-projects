/*                                                                                                                                                                                                                                 AK
Avinash Kumar   2/8/15
COP 3502C
9:30-10:20
Assignment was having people add, delete, or view the people on the list with their respected ids
This assignment taught how to use linked list with functions while reading in a file by making a function for it.
*/

// Libraries
#include<stdio.h>
#include<stdlib.h>

// Structure
struct node{
    char name[50];
    int id;
    struct node *next;

} *head;

//Functions
void read(int file);
void display(struct node * r);
void insert(char *insert_name, char * insert_num, int file);
int delete_id(int num);
int delete_name(char * delete_name);


//Main Function
int main(){
    char name_delete[50];
    char name_insert[50];
    char num[10];
    int i, id, file=0;
    struct node *n;


    //Initialize Linked List by setting to null
    head = NULL;

    // This reads the data file into the main
    read(file);
    file = 1;

    //Displays the menu and runs it in a loop
    while(1){
        printf("\nList Operations\n");
        printf("==================\n");
        printf("1. Insert\n");
        printf("2. Display\n");
        printf("3. Delete By ID\n");
        printf("4. Delete By Name\n");
        printf("5. Exit\n");
        printf("Enter your Choice : ");

        if(scanf("%d", &i) <= 0){
            printf("Enter only an integer from the menu.\n");
            exit(0);
        }
        else
        {
            //Carries out the users menu option using switches and cases
            switch(i)
            {

                case 1:
                    printf("Enter the name to insert: ");
                    scanf(" %[^\n]s", name_insert);
                    printf("What is the id number for %s? ");
                    scanf(" %[^\n]s", id);
                    insert(name_insert, id, file);
                    break;
                case 2:
                    if(head == NULL){
                        printf("List is Empty\n");
                    }
                    else{
                        printf("\nElement(s) in the list are :  ");
                        display(n);
                    }
                    break;
                case 3:
                    if(head == NULL)
                    printf("List is Empty\n");
                    else{
                        printf("Enter the id number to delete : ");
                        scanf("%d", &id);

                        if(delete_id(id))
                            printf("%d deleted successfully\n",id);
                        else
                            printf("%d not found in the list\n",id);
                    }
                    break;
                case 4:
                    if(head == NULL)
                    printf("List is Empty\n");
                    else
                    {
                        printf("Enter the name you want to delete : ");
                        scanf(" %[^\n]s", name_delete);

                        if(delete_name(name_delete))
                            printf("%s deleted successfully\n",name_delete);
                        else
                            printf("%s not found in the list\n",name_delete);
                    }
                    break;
                case 5:
                    return 0;
                default:
                    printf("Invalid option\n");

            }
        }
    }
    return 0;
}
//this function reads in the file so we can use it in the program accordingly
void read(int file)
{
    char * str, * sid;
    char array[50];
    char comma[2] = ",";
    char input_file[] = "AssignmentOneInput.txt";
    //opens file
    FILE *ifp;

    printf("Trying to open file %s\n", input_file);
    ifp = fopen(input_file, "r");

    if(ifp == NULL)
    {
       printf("There is no File to open");
    }
    //splits up names and id numbers
    while ((fgets(array, 50, ifp)) != NULL){
        str = strtok(array, comma);
        sid = strtok(NULL, comma);
        insert(str, sid, file);
    }
    //close file so we get no obscure errors
    fclose(ifp);
}

//This function just displays the list
void display(struct node *r){
    r = head;

    if(r == NULL){
        return;
    }

    while(r != NULL){
        printf("\n%s has id %d", r->name, r->id);
        r = r->next;
    }
    printf("\n");

}

//setting up the linked list with nodes and then plugging in users info of no people
//if the choose to add people to the list
void insert(char *insert_name, char *insert_num, int file){
    struct node *temp, * prev;
    temp = (struct node*)malloc(sizeof(struct node));

    int intId = atoi(insert_num);

    strcpy(temp->name, insert_name);
    temp->id = intId;
    temp->next = NULL;

    if(head == NULL)
        head = temp;

    else{
        prev = head;

            while(prev->next != NULL){
                prev = prev->next;
            }

            prev->next = temp;
    }
    if(file != 0)
        printf("%s with id number %s is added to the list.\n", insert_name, insert_num);

}
//finds the name the user has input searches the names that were in the file
//then if found deletes that person from the list
int delete_name(char *delete_name){
    struct node *temp, *prev;
    temp = head;

    printf("Checking for the name %s ...\n", delete_name);

    while(temp != NULL)
    {
        if(strcmp(temp->name, delete_name)==0){
        printf("Found %s!\n", delete_name);
            if(temp == head){
                head = temp->next;
                free(temp);
                return 1;
            }
            else{
                prev->next = temp->next;
                free(temp);
                return 1;
            }
        }
        else{
            prev = temp;
            temp = temp->next;
        }
    }
    return 0;
}
//Same thing as delete name except it deletes it using their id number instead of name
int delete_id(int num){
    struct node *temp, *prev;
    temp = head;

    while(temp != NULL){
        if(temp->id == num){
            if(temp == head){
                head = temp->next;
                head = (*temp).next;
                free(temp);
                return 1;
            }
            else{
                prev->next = temp->next;
                free(temp);
                return 1;
            }
        }
        else{
            prev = temp;
            temp = temp->next;
        }
    }
    return 0;
}

