/*                                                                                                                                                                                                                                 AK
Avinash Kumar   3/3/15
COP 3502C
9:30-10:20
Assignment was having us read in a file and then outputting it into a separate file
and also having the option to reverse the order of the list inputted from file
*/

//included libraries
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//structures for program
struct node
{
    char produce[60];
    char type[60];
    char soldBy[60];
    float price;
    int quantityInStock;
    struct node *next;
};

//Functions
struct node* read();
struct node* push(struct node* top, char x1[60], char *x2,char *x3, float x4, int x5);
void recursiveReverse(struct node** stock);
void display(struct node* r);
void export(struct node* r);

//Main Function
int main()
{
    //variables
    int i = 0;
    int choice;
    struct node* stock;

    //Displays the menu and runs it in a loop
    while(!i)
        {
        printf("List Operations\n");
        printf("===============\n");
        printf("1. Stock Produce Department\n");
        printf("2. Display Produce Inventory\n");
        printf("3. Reverse Order of Produce Inventory\n");
        printf("4. Export Produce Inventory\n");
        printf("5. Exit Program\n");
        printf("Enter your choice: ");

        //If statement to make sure user chooses option
        if(scanf("%d", &choice) <= 0)
        {
            printf("Enter only an integer from the menu.\n");
            exit(0);
        }
        else
        {
            //Carries out the users menu option using switches and cases
            switch(choice)
            {
                case 1:
                    printf("Attempting to open the file AssignmentTwoInput.txt\n");
                    stock = read();
                    printf("Successfully opened the file AssignmentTwoInput.txt\n");
                    break;
                case 2:
                    display(stock);
                    break;
                case 3:
                    recursiveReverse(&stock);
                    break;
                case 4:
                    printf("Attempting to create the file AssignmentTwoOutput.txt\n");
                    printf("Successfully wrote out the file AssignmentTwoOutput.txt\n");
                    export(stock);
                    break;
                case 5:
                    return;
            }
            printf("\n");
        }
    }
    return 0;
}
//Function is reading in the file
struct node* read()
{
    //Variables
    char line[60], x1[60], x2[60],  x3[60];
    FILE*ifp= fopen("AssignmentTwoInput.txt", "r");
    struct node* top= NULL;

    //Reads in the whole file and splits them up
    while(!feof(ifp))
        {
        fgets(line, 60, ifp);

        strcpy(x1, strtok(line, ","));
        strcpy(x2, strtok(NULL, ","));
        strcpy(x3, strtok(NULL, ","));

        float x4 = atof(strtok(NULL, ","));
        int x5 = atoi(strtok(NULL, ","));

        //This is to put the input file in the right order
        top = push(top, x1, x2, x3, x4, x5);
    }
    //close the file to avoid errors
    fclose(ifp);

    return top;
}
//Function places the data scanned into nodes
struct node* push(struct node* top, char x1[60], char x2[60],char x3[60], float x4, int x5)
{
    struct node* newNode = malloc(sizeof(struct node));

            //copies the scanned in variables into nodes
            strcpy(newNode->produce, x1);
            strcpy(newNode->type, x2);
            strcpy(newNode->soldBy, x3);
            newNode->price = x4;
            newNode->quantityInStock = x5;

            newNode->next = top;

      return newNode;
}
//Function displays the file in intended format
void display(struct node* r)
{
    struct node* category = r;
    int itemNumber = 0;

    //Make sure everything is aligned
    printf("========================================================================\n");
    printf(" Item #  Produce           Type             Sold By      Price  In Stock\n");
    printf("========================================================================\n");

    //Places all the nodes of the variables in the accurate place while spacing out
    //to fit the above header in accurate format
    while(category!= NULL){
        itemNumber++;
        printf("%-8d %-16s %-16s %-10s %7.2f %7d\n",itemNumber, category->produce, category->type, category->soldBy, category->price, category->quantityInStock);
        category= category->next;
    }
}
//Function reverses the stack
void recursiveReverse(struct node** stock)
{
    struct node* first;
    struct node* rest;

    //Makes sure it is not empty
    if (*stock == NULL)
       return;

    //recursion
    first = *stock;
    rest  = first->next;

    //Make sure it has nodes
    if (rest == NULL)
       return;

    //Reverses the list
    recursiveReverse(&rest);
    first->next->next = first;
    first->next  = NULL;

    //Fixes the header pointer
    *stock = rest;
}
//Function exports the file into desired file
void export(struct node* r)
{
    //Function copies display except puts into a .txt file
    struct node* category = r;
    int itemNumber = 0;
    FILE * ofp = fopen("AssignmentTwoOutput.txt", "w");

    fprintf(ofp,"========================================================================\n");
    fprintf(ofp," Item #  Produce           Type             Sold By      Price  In Stock\n");
    fprintf(ofp,"========================================================================\n");

    while(category!= NULL){
        itemNumber++;
        fprintf(ofp, "%-8d %-16s %-16s %-10s %7.2f %7d\n",itemNumber, category->produce, category->type, category->soldBy, category->price, category->quantityInStock);
        category = category->next;
    }
}
