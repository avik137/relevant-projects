/*                                                                                                                                                                                                                                 AK
Avinash Kumar   4/21/15
COP 3502C
9:30-10:20
Assignment was having us read in a file of numbers and using those
numbers to make and binary search tree and gave us and bunch of
different functions we can use it into by giving the user a menu and then
options to do them (some of the menu does not work such as median ,breadth first, and delete)
*/

//Included Libraries
#include<stdlib.h>
#include<stdio.h>

//Binary search tree node given to us
struct binTreeNode{

int data;
struct binTreeNode * right;
struct binTreeNode * left;

};

//Function Prototypes
struct binTreeNode* readtree();
void insert(struct binTreeNode ** tree, int val);
void print_preorder(struct binTreeNode * tree);
void print_inorder(struct binTreeNode * tree);
void print_postorder(struct binTreeNode * tree);
void deltree(struct binTreeNode * tree);
void print_breadth(struct binTreeNode * tree);
struct binTreeNode* enqueue(struct binTreeNode* q, int value);
struct binTreeNode* dequeue(struct binTreeNode* q);
int find(struct binTreeNode *current_ptr, int val);
int add(struct binTreeNode *current_ptr);
int count(struct binTreeNode *current_ptr);
int min_val(struct binTreeNode *current_ptr);
int max_val(struct binTreeNode *current_ptr);
int median(struct binTreeNode* current_ptr);
int store_array(struct binTreeNode* current_ptr);

void main(){

    //Variables
    int choice, stock, search_value, search_result, sum, count_value;
    int median_value, average, minimum, maximum;
    int i = 1;

    //Loop for the menu so the user can run multiple ones without having to run the whole function again
    while(i != 100){
        printf("\nSelect an option\n");
        printf("1. Generate Binary Search Tree.\n");
        printf("2. Print the BST in pre-order format.\n");
        printf("3. Print the BST in in-order format.\n");
        printf("4. Print the BST in post-order format.\n");
        printf("5. Print the BST in breadth-first format.\n");
        printf("6. Find a value in the BST.\n");
        printf("7. Find the minimum value in the BST nodes.\n");
        printf("8. Find the maximum value in the BST nodes.\n");
        printf("9. Find the average value of the BST nodes.\n");
        printf("10. Find the median value of the BST nodes.\n");
        printf("11. Calculate the sum of the BST nodes.\n");
        printf("12. Count the number of BST nodes.\n");
        printf("13. Delete a value in the BST.\n");
        printf("14. Exit program\n\n");

    scanf("%d", &choice);

//Conditions that take place depending on users input value form the menu
    switch(choice){

        case 1:
            stock = readtree();
            printf("Have successfully generated the BST.\n");
            break;
        case 2:
            print_preorder(stock);
            break;
        case 3:
            print_inorder(stock);
            break;
        case 4:
            print_postorder(stock);
            break;
        case 5:
            print_breadth(stock);
            break;
        case 6:
            //If statement to determine the result of the find function exists or not.
            printf("Enter a value you would like to search the BST for:\n");
            scanf("%d", &search_value);
            search_result = find(stock, search_value);
            if(search_result == 0){
                printf("The value is not in the BST\n");
            }
            if(search_result == 1){
                printf("The value is in the BST\n");
            }
            break;

        case 7:
            minimum = min_val(stock);
            printf("%d\n", minimum);
            break;
        case 8:
            maximum = max_val(stock);
            printf("%d\n", maximum);
            break;
        case 9:
            average = add(stock)/count(stock);
            printf("%d\n", average);
            break;
        case 10: //Did not figure out how to do successfully so just used break
        break;
        case 11:
            sum = add(stock);
            printf("%d\n", sum);
            break;
        case 12:
            count_value = count(stock);
            printf("%d\n", count_value);
            break;
        case 13: //Did not figure out how to do successfully so just used break
            break;
        case 14:
            return;
            }
        }

        return 0;
    }

//This reads in the file to get values for binary tree
struct binTreeNode* readtree(){

int val;
struct binTreeNode *tree = NULL;
struct binTreeNode *temp = NULL;
FILE*ifp = fopen("AssignmentFourInput.txt", "r");
    if(ifp == NULL){
        printf("Error\n");
    }
    else{
        while(!feof(ifp)){
            fscanf(ifp, "%d", &val);
            insert(&tree, val);
        }
    }
return tree;
}

//Inserts the values we read in to a binary tree
void insert(struct binTreeNode ** tree, int val)
{
    struct binTreeNode *temp = NULL;
    if(!(*tree))
    {
        temp = (struct binTreeNode *)malloc(sizeof(struct binTreeNode));
        temp->left = temp->right = NULL;
        temp->data = val;
        *tree = temp;
        return;
    }

    if(val < (*tree)->data)
    {
        insert(&(*tree)->left, val);
    }
    else if(val > (*tree)->data)
    {
        insert(&(*tree)->right, val);
    }

}

//Prints the binary tree in preorder format
void print_preorder(struct binTreeNode * tree)
{
    if (tree)
    {
        printf("%d\n",tree->data);
        print_preorder(tree->left);
        print_preorder(tree->right);
    }

}

//Prints the binary tree in inorder format
void print_inorder(struct binTreeNode * tree)
{
    if (tree)
    {
        print_inorder(tree->left);
        printf("%d\n",tree->data);
        print_inorder(tree->right);
    }
}

//Prints the binary tree postorder format
void print_postorder(struct binTreeNode * tree)
{
    if (tree)
    {
        print_postorder(tree->left);
        print_postorder(tree->right);
        printf("%d\n",tree->data);
    }
}

//Returns the pointer of the  searched value from the binary tree.
struct binTreeNode* search(struct binTreeNode** tree, int val){

if(!(*tree)){
    return NULL;
}

if(val < (*tree)->data){
    search(&((*tree)->left), val);
}
else if(val > (*tree)->data){
    search(&((*tree)->right), val);
}
else if(val == (*tree)->data){
    return *tree;
}
}


//Supposed to print the binary tree in a breadth-first format but does not work as intended
void print_breadth(struct binTreeNode * q){

    struct binTreeNode* value;
    enqueue(q, q->data);
    while(q != NULL){
        value = dequeue(q);
        printf("%d", value);
        enqueue(value->left, value->data);
        enqueue(value->right, value->data);
    }

}

//Removes the head of the binary tree and replaces it with the node after it.
struct binTreeNode* dequeue(struct binTreeNode* q){

    struct binTreeNode* temp = q;
    q = q->left;
    free(temp);

}

//Adds a node to the end of the binary tree.
struct binTreeNode* enqueue(struct binTreeNode* b, int value){
    struct binTreeNode* temp = b;

    if(b == NULL){
        b = malloc(sizeof(struct binTreeNode));
        b->data = value;
        b->left = NULL;
        b->right = NULL;
        return b;
    }

    else{
        while(b->left != NULL){
            b = b->left;
        }
        b->left = malloc(sizeof(struct binTreeNode));
        b->left->data = value;
        b->left->left = NULL;
    }
    return temp;
}

//Finds a specific value in the binary tree.
int find(struct binTreeNode *current_ptr, int val){
    if (current_ptr != NULL) {

        if (current_ptr->data == val){
            return 1;
        }
        if (val < current_ptr->data){
            return find(current_ptr->left, val);
        }
        else{
            return find(current_ptr->right, val);
        }
    }

    else{
    return 0;
    }
}

//Adds up all the values in the binary tree
int add(struct binTreeNode *current_ptr) {
    if (current_ptr != NULL)
        return current_ptr->data + add(current_ptr->left)+
        add(current_ptr->right);
    else
        return 0;
}

//Counts the number of nodes in our binary tree
int count(struct binTreeNode *current_ptr) {

    if (current_ptr != NULL)
        return 1 + count(current_ptr->left)+ count(current_ptr->right);
    else
        return 0;

}

//Finds minimum value in the Binary tree
int min_val(struct binTreeNode *current_ptr){
    if (current_ptr->left == NULL){
        return current_ptr->data;
    }
    else{
        return min_val(current_ptr->left);
    }
}

//Finds maximum value in the Binary tree
int max_val(struct binTreeNode *current_ptr){
    if (current_ptr->right == NULL){
        return current_ptr->data;
    }
    else{
        return max_val(current_ptr->right);
    }
}
