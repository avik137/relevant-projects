/*                                                                                                                                                                                                                                 AK
Avinash Kumar   3/28/15
COP 3502C
9:30-10:20
Assignment was having us read in a file of words then permutate those words
to get all the variants of the words possible using those letters and then
outputting the result into a file.
*/

//included libraries
#include <stdio.h>
#include <stdlib.h>

//Functions
void Swap(char str[], int i, int j);
void RecursivePermute(char str[], int k, FILE*ofp);

//Main Function
int main()
{
    //Pointer Files to read in and then output the words
    FILE*ifp= fopen("AssignmentThreeInput.txt", "r");
    FILE *ofp= fopen("AssignmentThreeOutput.txt", "w");

    //initialized
    char str[9];

    printf("Trying to open file AssignmentThreeInput.txt\n");

    //Loop to read in the words separately and runs them in the function
    while(!feof(ifp))
        {
        fscanf(ifp, "%s\n", str);
        RecursivePermute(str, 0, ofp);
        printf("Permutating %s \n\n", str);
        }

    printf("Processed input and wrote output! \n");
    //Closes File to make sure we get no unexpected errors
    fclose(ifp);
}
//This function runs the recursive so it it makes all the possible combinations
//of the words scanned in
void RecursivePermute(char str[], int k, FILE*ofp)
{
    //Initialized counter
    int j;

    //Print to output file
    if (k == strlen(str))
    {
        fprintf(ofp, "%s\n", str);
    }
    //The permutation the gets the different combination of the words
    else
    {
        for (j = k; j < strlen(str); j++)
        {
            Swap(str, k, j);
            RecursivePermute(str, k + 1, ofp);
            Swap(str, j, k);

        }
    }
}

//This function just swap two letter and this information is used in the
//recursive permute to get the different combinations of the word.
void Swap(char str[], int i, int j)
{
    char temp = str[i];
    str[i] = str[j];
    str[j] = temp;
}

