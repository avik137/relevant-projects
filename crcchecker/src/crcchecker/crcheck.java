//Avinash Kumar
//CIS 3360

package crcchecker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class crcheck {
	public static void main(String [] args) {
		String inputtest = new String();
		
		if(args[0].equals("c")){
			try{
				inputtest = getinputtext(args[1]);
			}
			catch(Exception e){
				System.out.println("An ascii character is not detected");
				e.printStackTrace();
			}
			calculatecrc(inputtest);
		}
		else if(args[0].equals("v")){
			try{
				inputtest = getinputtext(args[1]);
			}
			catch(Exception exc){
				System.out.println("An ascii character is not detected");
				exc.printStackTrace();
			}
			verification(inputtest);
		}
		else{
			System.out.println("Error first input must be \"c\" or \"v\"");
		}
	}
	public static void calculatecrc(String inputtest){
		System.out.println("CRC-16 calculation progress: \n");
		
		for(int i = inputtest.length(); i < 504; i++)
			inputtest += ".";
		
		int calculatedCRC = 0;
		String string2;
		for(int i = 0; i < 7; i++){
			string2 = inputtest.substring(i*64, (i+1)*64);
			calculatedCRC = calculatelinecrc(string2, calculatedCRC);
			
			System.out.print(string2 + " - ");
			int zeros = Integer.numberOfLeadingZeros(calculatedCRC)/4;
			for(int j = 0; j < zeros; j++)
				System.out.print("0");
			System.out.println(Integer.toHexString(calculatedCRC));
		}
		
		string2 = inputtest.substring(448, 504);
		calculatedCRC = calculatelinecrc(string2, calculatedCRC);
		
		System.out.print(string2);
		
		String crcstring = new String();
		int numZeros = Integer.numberOfLeadingZeros(calculatedCRC)/4;
		for(int j = 0; j < numZeros; j++)
			crcstring += "0";
		crcstring += Integer.toHexString(calculatedCRC);
		
		System.out.println(crcstring + " - " + crcstring + "\n");	
		
		System.out.println("CRC16 result : " + crcstring);
	}
	
	public static void verification(String text){
		System.out.println("CRC 16 calculation progress: \n");
		
		int thecalculatedcrc = 0;
		String substring;
		for(int i = 0; i < 7; i++){
			substring = text.substring(i*64, (i+1)*64);
			thecalculatedcrc = calculatelinecrc(substring, thecalculatedcrc);
			
			System.out.print(substring + " - ");
			
			int numZeros = Integer.numberOfLeadingZeros(thecalculatedcrc)/4;
			for(int j = 0; j < numZeros; j++)
				System.out.print("0");
			
			System.out.println(Integer.toHexString(thecalculatedcrc));
		}
		
		
		substring = text.substring(448, 504);
		thecalculatedcrc = calculatelinecrc(substring, thecalculatedcrc);
		
		
		System.out.print(text.substring(448, 512));
		
		String crcString = new String();
	
		int numZeros = Integer.numberOfLeadingZeros(thecalculatedcrc)/4;
		for(int j = 0; j < numZeros; j++)
			crcString += "0";
		crcString += Integer.toHexString(thecalculatedcrc);
		
	
		System.out.println(" - 	" + crcString + "\n");	
		
		System.out.println("CRC16 result is : " + crcString + "\n");
		
	
		System.out.print("CRC 16 verification is");
		if(crcString.equals(text.substring(504, 512)))
			System.out.println("passed");
		else 
			System.out.println("failed");
	}
	
	public static int calculatelinecrc(String s, int calculatedcrc){
		int valueofletter;
		
		for(int i = 0; i < s.length(); i+=2){
	
			valueofletter = s.charAt(i);
			valueofletter <<= 8;
			valueofletter |= s.charAt(i+1);
			
			valueofletter = calculateXOR(valueofletter, calculatedcrc<<1);
			
			calculatedcrc = calculateCRC(valueofletter);
		}
		
		
		return calculatedcrc;
	}
	
	public static int calculateCRC(int sixteenBits){
		int crcPoly = 0b1010000001010011;
		int crcResult = sixteenBits << 15;
		int numberofzeros = Integer.numberOfLeadingZeros(crcResult);
		int xorCRCValue;
		
		while(numberofzeros <= 16){
			xorCRCValue = crcPoly << (16-numberofzeros);
			crcResult = calculateXOR(crcResult, xorCRCValue);
			numberofzeros = Integer.numberOfLeadingZeros(crcResult);
		}
		return crcResult;
	}
	
	public static int calculateXOR(int a, int b){
		return a ^ b;
	}
	
	public static String getinputtext(String fileName) throws Exception{
		File file = new File(fileName);
		BufferedReader binary;
		
		String inputtest = new String();
		String buffer = new String();
		
		try{
			binary = new BufferedReader(new FileReader(file));
			while((buffer = binary.readLine()) != null)
				inputtest += buffer;
				
			binary.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		

		for(int i = 0; i < inputtest.length(); i++){
			if(inputtest.charAt(i) >= 128)
				throw new Exception();
		}
		
		return inputtest;
	}
}