/* Avinash Kumar
 * Section 3
 * 09/26/2014
 * Average Mile
 * Purpose to is to calculate how much it would take the user to run a mile and how many miles they can run in a mile.
 */

//included libraries
#include <stdio.h>
#define HOUR 60

//main function
int main() {
    //variable dictionary for this program
    int i, miles;
    double sum=0, average_time, time, miles_per_hour;

    //Asking user for the amount of miles they have run to see how many times the for loop has to run
    printf("How many miles did you run?\n");
    scanf("%d", & miles);

    //This is making a for loop so that it can account for various amounts of miles the user has ran
    for (i=1; i<=miles ; i++)
    {
        //Asking user for how long it took them to run each mile in minutes
        printf("How long did it take to run mile #%d in minutes\n", i);
        scanf("%lf", &time);

        //This adds up how many long it took them to run every mile.
        sum += time;

        //Calculates the average time and miles per hour using previous input of the user
        average_time = (double)sum/miles;
        miles_per_hour = HOUR/ average_time;
}
    //Results being printed out
    printf("\nYour average time to run a mile is %.3lf.\n",average_time);
    printf("This is approximately %.1lf miles per hour!\n",miles_per_hour);


return 0;
}
