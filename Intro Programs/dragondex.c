/* Avinash Kumar
 * Section 3
 * 11/24/2014
 * Dragon Collecting
 * This program lets you make your own dragon team by adding, removing, searching, and listing as long
 * you are using an input file to input commands and the dragons.
 * This program includes all the function which are: adding, removing, searching, and listing
 */
 //Included Libraries
#include <stdio.h>
#include <string.h>

//Given Values
#define MAX_LENGTH 40
#define MAX_TEAM 1000

//Given structs to store information
struct dragon {
    char name[MAX_LENGTH];
    char color[MAX_LENGTH];
};

struct collection {
    struct dragon team[MAX_TEAM];
    int num_dragons;
};
//Functions for add, remove, search, list
void add(struct collection * group, struct dragon * );
void dremove(struct collection * group, char * name);
int search(struct collection * group, char * name);
void collection(struct collection * group, char * color);

// Main function
int main() {
    //listed initializers
    char name[MAX_LENGTH], color[MAX_LENGTH], command[MAX_LENGTH];
    struct collection dragons;
    struct dragon dragon1;
    dragons.num_dragons = 0;
    //opening the input file
    FILE * ifp = fopen("dragon.txt", "r");
    int i;
    int num_commands;

    //Scanning the input file
    fscanf(ifp, "%d", &num_commands);

    //Loops the whole function as many times as there are commands
    for(i=0; i< num_commands; i++){
        fscanf(ifp, "%s", command);

        //If statements linking to the function
        if(strcmp(command, "ADD") == 0){
            fscanf(ifp, "%s", dragon1.name);
            fscanf(ifp, "%s", dragon1.color);
            add(&dragons, &dragon1);
        }
        else if (strcmp(command, "REMOVE") == 0){
            fscanf(ifp, "%s", name);
            dremove(&dragons, name);
            }
        else if(strcmp(command, "SEARCH") == 0){
            fscanf(ifp, "%s", name);
            search(&dragons, name);
        }
        else if(strcmp(command, "LIST") == 0){
            fscanf(ifp, "%s", color);
            list(&dragons, color);
        }
    }

  close(ifp);

  return 0;
}

void add(struct collection * group, struct dragon * dragon1){

    //Copies the name and color so it can be printed and stored
    strcpy( group->team[ group->num_dragons ].name, dragon1->name);
    strcpy( group->team[ group->num_dragons ].color, dragon1->color);

//  add dragon to group
    group->num_dragons++;

//  Output the added dragon
    printf("\n%s the %s dragon has been added to the team.\n", dragon1->name, dragon1->color);

    return;
}

void dremove(struct collection * group, char * name){
    //This is used to link to search because it gets the name and color from the search
    int set = search(group, name);

    //outputs the removed dragon from thee group
    printf("\n%s the %s dragon has been removed from the team.\n", group->team[set].name, group->team[set].color);

    //Removes the dragon from thee group
    group->team[set] = group->team[group->num_dragons - 1];

    //Minus the number of the dragon
    group->num_dragons--;

    return;

}

int search(struct collection * group, char * name){
   //initialized counter
    int i=0;

    //Loops all the listed dragons the team and searches for the dragon given
    for(i=0;i<group->num_dragons; i++){
        if( strcmp(name, group->team[i].name) == 0 ){
           //Outputs if the dragons is on the team
            printf("\n%s the dragon is currently on the team.\n", name);
            return i;
        }
    }
        //outputs if the dragon is not on the team
        printf("\n%s the dragon is currently NOT on the team.\n", name);

    return 0;
}

void list(struct collection * group, char * color){
    int i;

    printf("\n");

    //Outputs the color of the dragons list you want
    printf("%s dragons:\n", color);

    //loops the dragons so that we can see which ones are which color and compare them
    for (i=0; i<group->num_dragons; i++){
        if( strcmp( color, group->team[i].color ) == 0)
            //prints all the dragons of that color in the list
            printf("%s\n", group->team[i].name);
    }

    return;
}

