/* Avinash Kumar
 * Section 3
 * 09/12/2014
 * Calorie Counting
 * Purpose is to see how long it would take someone to achieve there desired goal based on calorie consumption and burned.
 */

 //included libraries
#include <stdio.h>

//main function
int main() {

    //Variables we need to use in this program
    int weight, goal, calories;
    int walk_day, work_day, food_day, drink_day;
    int walk, work, food, drink;
    int current_weight, time, week, day;

    //Ask user for the data to see how long it would take them to lose the weight based on their daily ways
    printf("Enter your current weight.\n");
    scanf("%d", & weight);

    printf("What is your weight goal?\n");
    scanf("%d", & goal);

    printf("How much do you spend walking in minutes?\n");
    scanf("%d", & walk_day);

    printf("How much time do you spend working out in minutes?\n");
    scanf("%d", & work_day);

    printf("How much time do you spend drinking in minutes?\n");
    scanf("%d", & drink_day);

    printf("How much time do you spend eating out in minutes?\n");
    scanf("%d", & food_day);

        //converting minutes of activity to calories per minute
        walk = walk_day * 5;
        work = work_day * 10;
        food = food_day * 40;
        drink = drink_day * 20;

    //this part calculates if they are trying to lose weight
    if (weight > goal){
        //Calculations
        calories = walk + work - food - drink;
        current_weight = weight - goal;
        time = (current_weight * 3500)/ calories;
        week = time / 7;
        day = time % 7;

        //results being printed out
        if (calories >= 0){
            printf("You burn %d calories per day.\n", calories);
            printf("You have %d more pounds to lose.\n", current_weight);
            printf("You will reach your goal weight in %d weeks and %d days\n", week, day);
    }
    //This is if they happened to being gaining weight daily instead of losing weight when they are trying to lose weight
    else
        printf("You are gaining weight every day, I recommend working out more often\n");
}
    //this part calculates if they wish to gain weight
    else {
        //calculations
        calories = food + drink - walk - work;
        current_weight = goal - weight;
        time = (current_weight * 3500)/ calories;
        week = time / 7;
        day = time % 7;

    //results being printed out
    if (calories >= 0){
        printf("You gain %d calories per day.\n", calories);
        printf("You have %d more pounds to gain.\n", current_weight);
        printf("You will reach your goal weight in %d weeks and %d days\n", week, day);
}
    //this is if they happened to losing weight daily when they are trying to gain weight
    else
        printf("You are losing weight every day, i recommend eating and drinking more every day.\n");
}


    return 0;
}
