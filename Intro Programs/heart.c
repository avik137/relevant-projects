/* Avinash Kumar
 * Section 3
 * 09/18/2014
 * Heart Rate
 * The purpose is to see how many times one person reached there target heart
 * rate out of the various heart rates recorded during there cardio workouts.
 */

//included libraries
#include <stdio.h>

//main function
int main() {
    //variable dictionary for this program
    int age, heart_rate, Target, total = 0, hits = 0;
    char gender;

    // Prompts user to input data to calculate their target heart rate
    printf("Enter your gender.\n");
    scanf("%c", & gender);

    printf("Enter your age.\n");
    scanf("%d", & age);

        //Calculation for male target heart zone.
        if (gender == 'M'|| gender == 'm')
            Target = 226 - age;

        //Calculation for female target heart zone.
        else if (gender == 'F' || gender == 'f')
            Target = 220 - age;

    //Loop for the various inputs of recorded heart rates.
    while (heart_rate != -1) {

        printf("Enter your recorded heart rates\n");
        scanf("%d", & heart_rate);

        //THis is to see how many times they hit their target heart zone out of the heart rates inputed
        if (heart_rate >= Target)
            hits++;

        //This is to see how many heart rates they inputed in total
        if (heart_rate != -1)
            total++;
    }
        //This prints the results of the number of times the target heart zone was hit.
        printf("You hit your target heart zone %d times out of %d\n", hits, total);


    return 0;
}
